<?php
Class BestHotel implements Hotel
{

    protected $fromDate;
    protected $toDate;
    protected $city;
    protected $numberOfAdults;

    function __construct( $fromDate, $toDate, $city, $numberOfAdults ) {
		$this->fromDate = $fromDate;
        $this->toDate = $toDate;
        $this->city = $city;
        $this->numberOfAdults = $numberOfAdults;
    }
    

    protected function getResults()
    {
        //Set from date to iso local format
        $from = DateTime::ISO8601($this->fromDate);

        //Set to date to iso local format
        $to = DateTime::ISO8601($this->toDate);

        //Calling the API
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"http://www.topHotels.com/api/city=AUH&adults=4&from=.'$from'.&to=.'$to'.");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close ($ch);

       //Return the response 
       $response = 
       [
           'hotel'=> 'Name of the Hotel',
           'hotelRate'=> 3,
           'hotelFare'=> 300.76,
           'discount'=> 10%,
           'roomAmenities'=> 'pool','Internet'
       ];

       return json_encode($response);
    }
 
}
?>